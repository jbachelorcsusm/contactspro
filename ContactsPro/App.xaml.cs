﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ContactsPro.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ContactsPro
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new ContactsProPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
