﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using ContactsPro.Models;
using Xamarin.Forms;

namespace ContactsPro.ViewModels
{
    // The name of each ViewModel is important:  Follow the convention of using the same name as the page, with "ViewModel" at the end.
    // This will make life easier when we start using an MVVM framework, like Prism: https://channel9.msdn.com/Shows/XamarinShow/Prism-for-XamarinForms-with-Brian-Lagunas 
    public class ContactsProPageViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        /// <summary>
        /// This event occurs when properties have changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NotifyPropertyChanged)}:  {propertyName}");
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged


        #region Command declarations

        public ICommand SaveContactCommand { get; set; }
        public ICommand ClearCommand { get; set; }


        #endregion Command declarations


        #region Properties & Fields

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get
            {
                return _selectedPerson;
            }
            set
            {
                if (value != _selectedPerson)
                {
                    _selectedPerson = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _shouldShowStatusText = false;
        public bool ShouldShowStatusText
        {
            get { return _shouldShowStatusText; }
            set
            {
                if (value != _shouldShowStatusText)
                {
                    _shouldShowStatusText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _statusText;
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (value != _statusText)
                {
                    _statusText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion Properties & Fields


        #region Constructor(s)

        public ContactsProPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ContactsProPageViewModel)}:  ctor");
            SaveContactCommand = new Command(OnSaveContact);
            ClearCommand = new Command(OnClear);

            SetUpViewModelForNewContact();
        }

        #endregion Constructor(s)


        #region ViewModel Methods

        private void OnSaveContact(object _) // As a convention, I use "_" to represent a parameter that has to be there, but I do not ever care about it.
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSaveContact)}");

            if (SelectedPerson.LikesPugs)
            {
                StatusText = $"Saved {SelectedPerson.FirstName} {SelectedPerson.LastName}!";
            }
            else
            {
                StatusText = $"Sorry, {SelectedPerson.FirstName}. Pug lovers only.";
            }

            ShouldShowStatusText = true;
        }

        private void OnClear(object _)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnClear)}");
            SetUpViewModelForNewContact();
        }

        private void SetUpViewModelForNewContact()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(SetUpViewModelForNewContact)}");

            SelectedPerson = new Person();
            ShouldShowStatusText = false;
            StatusText = string.Empty;
        }

        #endregion ViewModel Methods
    }
}
