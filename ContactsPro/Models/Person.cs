﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ContactsPro.Models
{
    /// <summary>
    /// Person class with change notification implemented. Check out the PersonWithoutChangeNotification class
    /// to see the simple version of this class without change notification.
    /// </summary>
    public class Person : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        /// <summary>
        /// This event occurs when properties have changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NotifyPropertyChanged)}");
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged


        #region Properties & Fields

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value != _firstName)
                {
                    _firstName = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value != _lastName)
                {
                    _lastName = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _likesPugs;
        /// <summary>
        /// A value of false here is truly unacceptable.
        /// </summary>
        /// <value><c>true</c> if likes pugs; otherwise, <c>false</c>.</value>
        public bool LikesPugs
        {
            get { return _likesPugs; }
            set
            {
                if (value != _likesPugs)
                {
                    _likesPugs = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if (value != _phoneNumber)
                {
                    _phoneNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DateTime _birthDay;
        public DateTime BirthDay
        {
            get { return _birthDay; }
            set
            {
                if (value != _birthDay)
                {
                    _birthDay = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _favoriteMovie;
        public string FavoriteMovie
        {
            get { return _favoriteMovie; }
            set
            {
                if (value != _favoriteMovie)
                {
                    _favoriteMovie = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (value != _email)
                {
                    _email = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion Properties & Fields
    }
}
