﻿using System;
namespace ContactsPro.Models
{
    /// <summary>
    /// This class is present for information only. It is here to show you what the 
    /// Person class would look like if implemented without any property change notifications.
    /// </summary>
    public class PersonWithoutChangeNotification
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool LikesPugs { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDay { get; set; }
        public string FavoriteMovie { get; set; }
        public string email { get; set; }
    }
}
