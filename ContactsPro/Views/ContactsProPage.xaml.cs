﻿using Xamarin.Forms;

namespace ContactsPro.Views
{
    public partial class ContactsProPage : ContentPage
    {
        public ContactsProPage()
        {
            InitializeComponent();

            /// This is a blissfully empty code-behind! It is the ideal that MVVM is shooting for.
            /// Debates rage over whether you should EVER put code in your code-behind.
            /// 
            /// In my opinion, the best approach is to treat the idea of an empty code-behind as an ideal to shoot for...
            /// NOT an absolute rule.
            /// 
            /// Sometimes you will need to add code to the code-behind to handle some specific requirement, and that's
            /// absolutely fine. Keep it as minimal as you can, but don't treat the code-behind as off-limits.
        }
    }
}
